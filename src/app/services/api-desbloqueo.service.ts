import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map, retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiDesbloqueoService {

  api_data: any;
  public httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json', 'accept':'application/json'})};
  public api_url = environment.Locations_api_url;

  constructor(
    private http: HttpClient
    ) { }

  postInsertLocation(api_consult:string, location: string, parent_id: string, area: string) {
    this.api_data = {
      "location":location,
      "parent_id":parent_id,
      "area":area
    };
    const datos = JSON.stringify(this.api_data);
    return this.http.post(this.api_url + api_consult, datos, this.httpOptions);
  }

  getAllowedUsers(api_consult:string, email: string) {
    let api_data;
    api_data = {
      "user":email,
    };
    const datos = JSON.stringify(api_data);
    return this.http.post(this.api_url + api_consult, datos);
  }

  public getLocations(api_consult:string) {
    return this.http.get(this.api_url+api_consult, this.httpOptions).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
