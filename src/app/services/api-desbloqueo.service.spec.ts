import { TestBed } from '@angular/core/testing';

import { ApiDesbloqueoService } from './api-desbloqueo.service';

describe('ApiDesbloqueoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiDesbloqueoService = TestBed.get(ApiDesbloqueoService);
    expect(service).toBeTruthy();
  });
});
