import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AuthenticationService } from '../services/authentication.service';
import { ApiDesbloqueoService } from '../services/api-desbloqueo.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 
  project: any = [];
  locations:any = [];
  public myForm: FormGroup;
  nameButton: any;
  user:any = this.auth.currentUserObservable.currentUser;


  constructor(
    public auth: AuthenticationService,
    private api:ApiDesbloqueoService,
    private  formBuilder: FormBuilder,
    ) { }

  ngOnInit() {
 
    this.api.getLocations('location').subscribe(
      (result:any) => {
        if(result.body){
          this.locations = [];
          for(let i=0;i<result.body.length;i++){
            this.locations.push(result.body[i]);
          }
          console.log(result.body);
        }
      },
   );

    this.myForm = this.formBuilder.group({
      location: ['', [Validators.required, Validators.minLength(1), Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g)]],
      area: ['', [Validators.required, Validators.minLength(1)]],
      parent_id: ['',[Validators.required]],

     });
  }
  get f() { return this.myForm.controls; }


  onSubmit(){

    const form = this.myForm.value;
    if(form.parent_id==""){
      this.myForm.controls.parent_id.setValue("Na");
    }
    console.log(this.myForm.controls.parent_id)

    if(this.myForm.valid){
      this.nameButton = 'Enviando';
      this.api.postInsertLocation( 'location', form.location, form.parent_id, form.area).subscribe(
          result => {
            if(result){
              Swal.fire({
                title: 'Registro ingresado exitosamente',
                showConfirmButton: true,
                confirmButtonText: 'Entendido',
                confirmButtonColor: '#008000'
              });
              this.myForm.reset();
            }
          },
          error => {
            Swal.fire({
              title: 'Error API.'
            });
          });
      this.nameButton = 'Enviar';
    }else if(this.myForm.invalid){
      console.log(this.myForm);
      Swal.fire({
        title: 'Formulario inválido',
        text: 'Diligencie todos los campos del formulario',
        cancelButtonText: 'Ok'
      });
    }
  }
}
