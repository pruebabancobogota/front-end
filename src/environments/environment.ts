// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD-uYq4_dCZm2y1eL-fAf1nu79YyFJ4x1w",
    authDomain: "desbloqueo-usuarios-mind-cloud.firebaseapp.com",
    databaseURL: "https://desbloqueo-usuarios-mind-cloud.firebaseio.com",
    projectId: "desbloqueo-usuarios-mind-cloud",
    storageBucket: "",
    messagingSenderId: "285019118535"
  },
  
  Locations_api_url: "http://localhost:3002/",
};

//     appId: "1:285019118535:web:db9e194d4a19da22"
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
